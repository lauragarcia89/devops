Crear cuenta
Pinchamos en los 9 puntitos y vamos a "Cuenta"
Una vez dentro pintachos en el menu lateral sobre la opción "Seguridad"
Dentro de "Seguridad" bajamos hasta abajo del todo y pinchamos sobre "Acceso de aplicaciones poco seguras"
Dentro habilitamos el check de "Permitir el acceso de aplicaciones poco seguras"
Y listo, ya podemos enviar emails desde Jenkins.