Docker (Pagina web principal de Comamndos para docker)
https://docs.docker.com/engine/reference/commandline/cli/

Listar imagenes
docker images                                   (listar imagenes | opcion 1)
docker image ls                                 (listar imagenes | opcion 2)
docker rmi [IMAGE ID]                           (eliminar imagen, siempre y cuando no tenga un contenedor ni activo ni parado)
docker rmi -f [IMAGE ID]                        (eliminar imagen con un contenedor parado)

Borrado de todas las imagenes en las que conincide el nombre
docker images
docker images | grep [REPOSITORY] | awk '{print $3}' | xargs docker rmi -f

Listar contenedores (a partir de una imagen)
docker ps                                       (listar contenedores | opcion 1)
docker ps -a                                    (muestra tanto activos como inactivos)
docker container ls                             (listar contenedores | opcion 2)
docker stop [CONTAINER ID]                      (parar un contenedor)
docker rm [CONTAINER ID]                        (eliminar contenedor parado)
docker restart [CONTAINER ID]                   (reactivar un contenedor)

Borrado de todos los contenedores
docker ps -a
docker stop [CONTAINER ID] && docker rm [CONTAINER ID]
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)

Listar volumenes (archivos de configuración de los contenedores)
docker volume ls                                (listar archivos de configuración donde tenemos los plugins)
docker volume create [NOMBRE]                   (crear un nuevo archivo de configuración para la imagen)
docker volume rm [NOMBRE]                       (eliminar el archivo de configuración)

Listar redes (redes propias de comunicación entre imagenes)
docker network ls                               (listar archivos de red privada/publica donde tenemos los plugins)
docker network create [NETWORK ID | NOMBRE]     (crear un nuevo archivo de red privada/publica para la imagen)
docker network rm [NETWORK ID | NOMBRE]         (eliminar el archivo de red privada/publica)
-----------------------------------------------------------------------------

Ver el contenido de una imagen
docker run -it [IMAGE:TAG] bash

Ver el contenido de un contenedor
docker exec -ti [CONTAINER ID] bash

Levantar un contenedor a partir de una imagen
docker run -d -p 8123:8080 [REPOSITORY | REPOSITY:TAG]
docker run -d -p 8066:8080 jenkinsci/blueocean          (Ejemplo)
-----------------------------------------------------------------------------

Docker build
docker build -t [REPOSITORY:TAG] .                      (El punto indica donde se encuantra el archivo Dockerfile)

Subir imagen a DockerHub
docker tag [REPOSITORY:TAG] [USERNAME/REPOSITORY:TAG]   (El slash del medio es necesario, retageamos docker local a docker cloud)
docker push [USERNAME/REPOSITORY:TAG]                   (Hacemos la subido a Docker Hub)