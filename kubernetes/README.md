Url instalación
https://minikube.sigs.k8s.io/docs/start/
Azure       - https://azure.microsoft.com/en-us/topic/what-is-kubernetes/#features
Google      - https://cloud.google.com/kubernetes-engine
AWS         - https://aws.amazon.com/es/eks/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc&eks-blogs.sort-by=item.additionalFields.createdDate&eks-blogs.sort-order=desc
OpenShift   - https://www.openshift.com/blog/enterprise-kubernetes-with-openshift-part-one

Minikube
minikube start
--network-plugin=cni
--enable-default-cni
--container-runtime=containerd
--bootstrapper=kubeadm

-------------------------------------------------------------------------------------

Commands
docker login
minikube start                                      (start minikube)
minikube dashboard                                  (launch dashboard)
minikube ip                                         (navigator ip access)
minikube docker-env                                 (enable docker images in minikube)
minikube stop                                       (stop minikube)
minikube delete                                     (delete minikube)
minikube ssh                                        (watch in terminal like linux)

-------------------------------------------------------------------------------------

Subcommands
https://github.com/h0x91b/playing-docker/tree/master/kubernetes
kubectl apply -f deployment.yml                     (deploy docker image)
kubectl get deployments                             (list deployments)
kubectl delete -f deployment.yml                    (clean up)
kubectl get pods                                    (list pods "docker images")
kubectl exec -it NAME -- /bin/bash                  (see pod content)
kubectl describe pod NAME                           (see pod info)
kubectl logs NAME                                   (see pod logs)
tail -f logs.log                                    (see logs if is configured)
kubectl apply -f service.yml                        (deploy service)
kubectl get svc                                     (list services)
kubectl describe svc NAME                           (see service info)
minikube addons enable ingress                      (enable ingress for working with local ips)
kubectl apply -f ingress.yml                        (deploy ingresses)
open $(printf "http://%s/foo" $(minikube ip))       (Open /foo via ingress)
kubectl get events                                  (list events)
kubectl get nodes                                   (list nodes)
kubectl describe node NAME                          (see info: cpu, memory)
kubectl apply -f cron.yml                           (deploy cron)
kubectl get cronjobs                                (list crones)
kubectl describe cronjob NAME                       (see cronjob info)
kubectl apply -f secret.yml                         (deploy secret)
kubectl get secrets                                 (list secrets)
kubectl describe secret NAME                        (see secret info)